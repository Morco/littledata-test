/**
 * Utility functions for all problems
 */

const fs = require('fs');
const path = require('path');


let inputFilePath;

module.exports = {
	/**
	 * Get input data from input file
	 */
	getInputData() {
		// get file name
		inputFilePath = process.argv[2];
		if (!inputFilePath) {
			console.log('ERROR: Missing input path');
			process.exit();
		}

		// read file data
		let inputData;
		try {
			inputData = fs.readFileSync(inputFilePath, 'utf8');
		}
		catch (err) {
			console.log('ERROR: Cannot read file: ' + inputFilePath);
			process.exit();
		}

		return inputData;
	},

	/**
	 * Write output data to output file
	 */
	setOutputData(outputData) {
		// get output directory
		const dir = path.dirname(inputFilePath);
		// get input file name
		const fileName = path.basename(inputFilePath);
		// create output path
		const outputFilePath = dir + '/' + fileName + '.answer';

		// write output to file
		fs.writeFileSync(outputFilePath, outputData, (err) => {
			if (err) {
				console.log('ERROR: Cannot write file: ' + outputFilePath);
			}
		});
	},
};
