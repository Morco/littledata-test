/**
 * Solution for Problem 2: Pathfinding
 */

const utils = require('./utils');
const VerEx = require('verbal-expressions');
const PF = require('pathfinding');


// get input data
const inputData = utils.getInputData();


// parse input and create map
const pirateMap = [];

// extract coordinates
// split by new line, space and comma
let coords = inputData.split(/[\n\s,]+/);

// filter wrong coordinates
const tester = VerEx()
	.then('x')
	.digit().oneOrMore()
	.then('y')
	.digit().oneOrMore()
	.removeModifier('g');
coords = coords.filter(value => {
	return tester.test(value);
});


// utility function for getting position from coords
function getPosition(value) {
	const [_, x, y] = value.split(/[xy]+/).map(e => Number(e));
	return [x, y];
}


// add start and end positions
const [xS, yS] = getPosition(coords[0]);
const [xE, yE] = getPosition(coords[coords.length - 1]);
// check NaN
if (isNaN(xS) || isNaN(yS) || isNaN(xE) || isNaN(yE)) {
	console.log('ERROR: Missing start or end positions');
	process.exit();
}
// check same position
if (xS === xE && yS === yE) {
	console.log('ERROR: Start and End positions cannot be the same');
	process.exit();
}
// update map
pirateMap[yS] = [];
pirateMap[yS][xS] = 'S';
pirateMap[yE] = [];
pirateMap[yE][xE] = 'E';


// add reefs to map
// also calculate map size
let xMax = 0;
let yMax = 0;
for (let i = 1; i < coords.length - 1; i += 1) {
	// get position
	const [x, y] = getPosition(coords[i]);
	// add to map
	if (!pirateMap[y]) {
		pirateMap[y] = [];
	}
	pirateMap[y][x] = 'x';
	// update map size
	xMax = (x >= xMax ? (x + 1) : xMax);
	yMax = (y >= yMax ? (y + 1) : yMax);
}


// fill the rest of the map
for (let x = 0; x < xMax; x += 1) {
	for (let y = 0; y < yMax; y += 1) {
		if (!pirateMap[y]) {
			pirateMap[y] = [];
		}
		if (!pirateMap[y][x]) {
			pirateMap[y][x] = '.';
		}
	}
}


// prepare path finder
const grid = new PF.Grid(xMax, yMax);
// mark reefs as blocking
for (let x = 0; x < xMax; x += 1) {
	for (let y = 0; y < yMax; y += 1) {
		if (pirateMap[y][x] === 'x') {
			grid.setWalkableAt(x, y, false);
		}
	}
}

// find path
const finder = new PF.AStarFinder();
const path = finder.findPath(xS, yS, xE, yE, grid);


// check if any path found
if (path.length > 0) {
	// update pirate map with path
	for (let i = 1; i < path.length - 1; i += 1) {
		const [x, y] = path[i];
		pirateMap[y][x] = 'O';
	}

	// generate output data
	let outputData = '';
	for (let y = 0; y < yMax; y += 1) {
		for (let x = 0; x < xMax; x += 1) {
			outputData += pirateMap[y][x];
		}
		// add new line
		if (y < yMax - 1) {
			outputData += '\n';
		}
	}

	// write output to file
	utils.setOutputData(outputData);
}
else {
	// write error
	utils.setOutputData('error');
}
