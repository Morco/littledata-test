/**
 * Solution for Problem 1: Hash Iterator
 */

const md5 = require('md5');
const utils = require('./utils');


// get input data
const inputData = utils.getInputData();

// parse input
let inputSalt;
let inputX;
try {
	[inputSalt, inputX] = inputData.split(',');
	inputX = Number(inputX);
	// validate input
	if (!inputSalt || !inputX) {
		throw null;
	}
}
catch (err) {
	console.log('ERROR: Wrong input format: ' + inputData);
	process.exit();
}


// prepare output
let outputMap = [];
const outputLength = 10;
let saltIter = 0;
const zeros = '0'.repeat(inputX);

while (true) {
	// prepare salt
	saltIter += 1;
	const salt = inputSalt + saltIter;

	// create MD5
	const saltMD5 = md5(salt).toString();

	// sanity check
	if (saltMD5.length < inputX - 1) {
		console.log('ERROR: MD5 requires more characters: ' + saltMD5);
		process.exit();
	}

	// check the starting 0's
	if (saltMD5.substring(0, inputX) === zeros) {
		// MD5 found

		// find index in output
		const outputSlot = Number(saltMD5[inputX]);
		// check if number
		if (outputSlot || outputSlot === 0) {
			// calculate char index
			const charIndex = saltIter % saltMD5.length;
			// check if slot is available
			if (!outputMap[outputSlot]) {
				// add char to output
				outputMap[outputSlot] = saltMD5[charIndex];

				// check if output is ready
				let currentLength = 0;
				for (let i = 0; i < outputLength; i += 1) {
					if (outputMap[i]) {
						currentLength += 1;
					}
				}
				if (outputLength === currentLength) {
					// output ready
					break;
				}
			}
		}
	}

	// continue loop
}


// generate final output
let outputData = '';
for (let i = 0; i < outputLength; i += 1) {
	outputData += outputMap[i];
}

// write output to file
utils.setOutputData(outputData);
